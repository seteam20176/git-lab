program project1;

const
     Size = 20;
     RangeA = 11;
     RangeB = 12;

type
     Matrix = array [1..Size] of array [1..Size] of integer;
     TaskLine = array [1..Size] of integer;

var
    MatrixA, MatrixB:Matrix;
    i, Min, Line, Column, Sum:integer;
    SequenceC, SequenceD:TaskLine;

    function GetRandomMatrix(_range: integer):Matrix;
             var _i, _j:integer;
                 _matrix:Matrix;
             Begin
                 for _i := 1 to Size do
                 Begin
                     for _j := 1 to Size do
                     Begin
                          _matrix[_i, _j] := random(_range*2 + 1) - _range;
                     End;
                 End;
                 GetRandomMatrix := _matrix;
             End;

procedure PrintMatrix(_matrix: Matrix);
          var _i, _j: integer;
          Begin
              for _i := 1 to Size do
              Begin
                   for _j := 1 to Size do
                   Begin
                        write(_matrix[_i, _j]:4);
                   End;
                   writeln;
              End;
              writeln;
          End;

function GetElementForSequenceC(_lineOfMatrixA, _lineOfMatrixB: TaskLine): integer;
         var _i: integer;
             _areSmaller: boolean;
         Begin
              _areSmaller := true;
              for _i := 1 to Size do
              Begin
                   if (_lineOfMatrixA[_i] >= _lineOfMatrixB[_i]) then _areSmaller := false;
              End;
              if _areSmaller = true then GetElementForSequenceC := 1
              else GetElementForSequenceC := 0;
         End;

function GetElementForSequenceD(_lineOfMatrixA, _lineOfMatrixB: TaskLine): integer;
         var _i, _numberOfPositiveElements, _numberOfNegativeElements: integer;
         Begin
              _numberOfPositiveElements := 0;
              _numberOfNegativeElements := 0;
              for _i := 1 to Size do
              Begin
                   if _lineOfMatrixA[_i] >= 0 then _numberOfPositiveElements := _numberOfPositiveElements + 1;
                   if _lineOfMatrixB[_i] < 0 then _numberOfNegativeElements := _numberOfNegativeElements + 1;
              End;
              if _numberOfPositiveElements = _numberOfNegativeElements then GetElementForSequenceD := 1
              else GetElementForSequenceD :=0 ;
         End;

procedure SearchMinDiagonalElement( _matrix: Matrix; var _line, _column, _min : integer);
          var _i, _j: integer;
          Begin
               _min := 20;
               _i := 1;

               while _i <= Size do
               Begin
                    if _matrix[_i][_i] < _min then
                    Begin
                    _min := _matrix[_i][_i];
                    _line := _i;
                    _column := _i;
                    End;
                    _i := _i + 1;
               End;
          End;

procedure ChangeMinAndEverageSum(var _matrix: Matrix; _line, _column: integer; _min, _everageSum: integer);
          var _i, _j: integer;
          Begin
               _matrix[_line, _column] = _everageSum;
          End;

function GetEverageSumOfDiagonalElements(_matrix: Matrix):real;
         var _i, _j,_count,_sumOfDiagonalElements: integer;
         Begin
              _sumOfDiagonalElements:=0;
              _count:=0;
              for _i:=1 to Size do
                  for _j:=1 to Size do
                  Begin
                       if(_i+_j>Size+1) then
                       Begin
                            _sumOfDiagonalElements:=_sumOfDiagonalElements+_matrix[i,j];
                            _count:=_count+1;

                       end;
                  end;
              GetEverageSumOfDiagonalElements:=_sumOfDiagonalElements/_count;
         End;




Begin
  randomize;
  MatrixA := GetRandomMatrix(RangeA);
  MatrixB := GetRandomMatrix(RangeB);

  writeln('Matrix A: ');
  printMatrix(MatrixA);

  writeln('**************************************************');
  writeln('Matrix B: ');
  printMatrix(MatrixB);

  for i := 1 to Size do
  Begin
       SequenceC[i] := GetElementForSequenceC(MatrixA[i], MatrixB[i]);
  End;

  write('Sequence C= ');
  for i := 1 to Size do
  Begin
       write(SequenceC[i]:2);
  End;

  writeln;
  writeln;
  writeln('1 if all elements in line i of Matrix A are smaller than elements in line i of Matrix B, else 0');
  writeln;
  writeln('*****************************');
  writeln;

  for i := 1 to Size do
  Begin
       SequenceD[i] := GetElementForSequenceD(MatrixA[i], MatrixB[i]);
  End;

  write('Sequence D= ');

  for i := 1 to Size do
  Begin
       write(SequenceD[i]:2);
  End;

  writeln;
  writeln;
  writeln('1 if the number of positive elements in line i of Matrix A is equivalent to the number of negative elements in line i of Matrix B, else 0');

  readln;
End.


